#Windows 8.1 Developers Contest Sample Application

This is the sample application for my **Effective Architecture of Modern Applications** session at an event organized by the local Microsoft DPE team.

Branches represent steps in the development of the application in the order I presented them during the talk. Each of them introduces a single feature or development approach.

- **1-BasicMVVM** contains only the basic application framework following the MVVM pattern with very basic model, view and view model classes.
- **2-DesignTimeModel** adds an alternative model implementation for the designer.
- **3-IoCContainer** uses Unity as an IoC container to simplify the view model instantiation and make its maintenance easier when the project grows.
- **4-UnitTesting** has an additional testing project in the solution for unit testing the application.
- **5-Mocking** demonstrates a simplistic approach to mocking for dependencies of the tested class.
- **6-PortableLibrary** introduces a portable class library project with the model classes moved to it so that they can be used on multiple platforms.

The best way to take a closer look at individual branches is to make a local clone of the repository and switch to the branch you are interested in. If you don't have a Git client installed you can download the source for each of the branches if you click on the [Download](https://bitbucket.org/damirarh/win81devcontest/downloads) link and then switch to the *Branches* tab.

There are multiple NuGet packages used in the sample. With NuGet 2.7 or later they should be automatically downloaded when you build the project for the first time.
