﻿using System;
using System.Collections;
using System.Threading;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using Win81DevContest.Portable.Models;
using Win81DevContest.Tests.Mocks;
using Win81DevContest.WinStore.ViewModels;

namespace Win81DevContest.Tests
{
    [TestClass]
    public class ApplicationViewModelTests
    {
        [TestMethod]
        public void ApplicationsOrderedCorrectly()
        {
            var repository = new MockContestRepository
            {
                Applications = new[]
                {
                    new ContestApp {Id = 1},
                    new ContestApp {Id = 3},
                    new ContestApp {Id = 2}
                }
            };
            var expected = new[]
            {
                new ContestApp {Id = 3},
                new ContestApp {Id = 2},
                new ContestApp {Id = 1}
            };

            var viewModel = new ApplicationsPageViewModel(repository);
            if (viewModel.Applications == null)
            {
                var handle = new AutoResetEvent(false);
                viewModel.PropertyChanged += (sender, args) => handle.Set();
                handle.WaitOne(TimeSpan.FromSeconds(1));
            }
            
            CollectionAssert.AreEqual(expected, viewModel.Applications, new ContestAppComparer());
        }

        private class ContestAppComparer : IComparer
        {
            public int Compare(object x, object y)
            {
                var appX = x as ContestApp;
                var appY = y as ContestApp;
                return (appX != null) 
                    && (appY != null) 
                    && (appX.Id == appY.Id) 
                    && (appX.Name == appY.Name) 
                    && (appX.ImageUri == appY.ImageUri) ? 
                        0 : -1;
            }
        }
    }
}
