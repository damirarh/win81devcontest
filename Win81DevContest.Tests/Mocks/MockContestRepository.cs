﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Win81DevContest.Portable.Models;

namespace Win81DevContest.Tests.Mocks
{
    public class MockContestRepository : IContestRepository
    {
        public IEnumerable<ContestApp> Applications { get; set; }
        public Task<IEnumerable<ContestApp>> GetApplicationsAsync()
        {
            return Task.FromResult(Applications);
        }
    }
}