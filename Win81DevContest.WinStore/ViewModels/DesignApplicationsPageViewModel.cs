﻿using Win81DevContest.Portable.Models;

namespace Win81DevContest.WinStore.ViewModels
{
    public class DesignApplicationsPageViewModel : ApplicationsPageViewModel
    {
        public DesignApplicationsPageViewModel() 
            : base(new DesignContestRepository())
        { }
    }
}