﻿using System.Collections.ObjectModel;
using System.Linq;
using Microsoft.Practices.Prism.StoreApps;
using Win81DevContest.Portable.Models;

namespace Win81DevContest.WinStore.ViewModels
{
    public class ApplicationsPageViewModel : ViewModel
    {
        private ObservableCollection<ContestApp> _applications;
        private readonly IContestRepository _repository;

        public ObservableCollection<ContestApp> Applications
        {
            get
            {
                return _applications;
            }
            private set
            {
                SetProperty(ref _applications, value);
            }
        }

        public ApplicationsPageViewModel(IContestRepository repository)
        {
            _repository = repository;
            Init();
        }

        private async void Init()
        {
            Applications = new ObservableCollection<ContestApp>((await _repository.GetApplicationsAsync()).OrderByDescending(a => a.Id));
        }
    }
}