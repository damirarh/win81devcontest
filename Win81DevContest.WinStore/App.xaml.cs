﻿using System.Threading.Tasks;
using Microsoft.Practices.Prism.StoreApps;
using Windows.ApplicationModel.Activation;
using Microsoft.Practices.Unity;
using Win81DevContest.Portable.Models;

namespace Win81DevContest.WinStore
{
    sealed partial class App : MvvmAppBase
    {
        private readonly IUnityContainer _container = new UnityContainer();

        public App()
        {
            this.InitializeComponent();
        }

        protected override Task OnLaunchApplication(LaunchActivatedEventArgs args)
        {
            NavigationService.Navigate("Applications", null);
            return Task.FromResult<object>(null);
        }

        protected override void OnInitialize(IActivatedEventArgs args)
        {
            _container.RegisterType<IContestRepository, ContestRepository>();

            ViewModelLocator.SetDefaultViewModelFactory(viewModelType => _container.Resolve(viewModelType));
        }
    }
}
