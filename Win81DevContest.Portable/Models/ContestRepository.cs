﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace Win81DevContest.Portable.Models
{
    public class ContestRepository : IContestRepository
    {
        public async Task<IEnumerable<ContestApp>> GetApplicationsAsync()
        {
            var contestPageSource = await GetPageSourceAsync(new Uri("http://winappdevcon.promorc.com/Home/Applications"));
            return ScrapeAppsFromPage(contestPageSource);
        }

        private async Task<string> GetPageSourceAsync(Uri pageUri)
        {
            var httpClient = new HttpClient();
            return await httpClient.GetStringAsync(pageUri);
        }

        private IEnumerable<ContestApp> ScrapeAppsFromPage(string pageSource)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(pageSource);
            return doc.DocumentNode.LastChild.ChildNodes.Single(n => n.OriginalName == "body")
                                             .ChildNodes.Single(n => n.OriginalName == "div" && n.Attributes.Any(a => a.OriginalName == "class" && a.Value == "body-content"))
                                             .ChildNodes.Single(n => n.OriginalName == "div")
                                             .ChildNodes.First(n => n.OriginalName == "div")
                                             .ChildNodes.First(n => n.OriginalName == "div")
                                             .ChildNodes.Single(n => n.OriginalName == "div" && n.Attributes.Any(a => a.OriginalName == "class" && a.Value == "row-fluid"))
                                             .ChildNodes.Single(n => n.OriginalName == "div")
                                             .ChildNodes.Where(n => n.OriginalName == "div" && n.Attributes.Any(a => a.OriginalName == "id" && a.Value == "applications_8"))
                                             .SelectMany(n => n.ChildNodes.Where(c => c.OriginalName == "div"))
                                             .Select(ScrapeAppDataFromFragment);
        }

        private ContestApp ScrapeAppDataFromFragment(HtmlNode rootNode)
        {
            var rootedUriString = rootNode.ChildNodes.Single(n => n.OriginalName == "div" && n.Attributes.Any(a => a.OriginalName == "class" && a.Value == "applications_3"))
                                          .ChildNodes.Single(n => n.OriginalName == "img")
                                          .Attributes.Single(a => a.OriginalName == "src").Value;
            var detailsNode = rootNode.ChildNodes.Single(n => n.OriginalName == "div" && n.Attributes.Any(a => a.OriginalName == "class" && a.Value == "applications_4"))
                                      .ChildNodes.Single(n => n.OriginalName == "ul")
                                      .ChildNodes.First(n => n.OriginalName == "li")
                                      .ChildNodes.Single(n => n.OriginalName == "a");
            return new ContestApp
            {
                ImageUri = new Uri("http://winappdevcon.promorc.com" + rootedUriString),
                Name = detailsNode.InnerText,
                Id = Int32.Parse(detailsNode.Attributes.Single(a => a.OriginalName == "href").Value.Split('/').Last())
            };
        }
    }
}