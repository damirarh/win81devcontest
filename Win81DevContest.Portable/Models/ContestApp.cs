﻿using System;

namespace Win81DevContest.Portable.Models
{
    public class ContestApp
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Uri ImageUri { get; set; }
    }
}