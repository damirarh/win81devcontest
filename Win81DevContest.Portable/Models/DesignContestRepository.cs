﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Win81DevContest.Portable.Models
{
    public class DesignContestRepository : IContestRepository
    {
        public Task<IEnumerable<ContestApp>> GetApplicationsAsync()
        {
            IEnumerable<ContestApp> applications = new[]
            {
                new ContestApp {Id = 1, Name = "Application 1", ImageUri = new Uri("ms-appx:///Assets/Logo.scale-100.png")},
                new ContestApp {Id = 2, Name = "Application 2", ImageUri = new Uri("ms-appx:///Assets/Logo.scale-100.png")},
                new ContestApp {Id = 3, Name = "Application 3", ImageUri = new Uri("ms-appx:///Assets/Logo.scale-100.png")},
                new ContestApp {Id = 4, Name = "Application 4", ImageUri = new Uri("ms-appx:///Assets/Logo.scale-100.png")},
                new ContestApp {Id = 5, Name = "Application 5", ImageUri = new Uri("ms-appx:///Assets/Logo.scale-100.png")}
            };
            return Task.FromResult(applications);
        }
    }
}