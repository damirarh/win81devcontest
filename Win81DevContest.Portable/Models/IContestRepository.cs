﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Win81DevContest.Portable.Models
{
    public interface IContestRepository
    {
        Task<IEnumerable<ContestApp>> GetApplicationsAsync();
    }
}